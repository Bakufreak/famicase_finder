**Famicase Finder:**

An easy (optionally offline) way to browse Famicase carts, intended for participants of the AGBIC gamejam.

---

**Ripping data from Famicase:**

- Easiest way to pull text data (name, designer, description) is via the Chronicle's XML files ([example](https://famicase.com/chronicle/data2019.xml)). Unfortunately (current year)'s designs don't get added to the Chronicle immediately, so instead data can be easily scraped with extensions like [Web Scraper](https://webscraper.io/):
	- Start URL example: `https://famicase.com/20/softs/[001-270].html`
	- Selectors: name, author, description and cart_img
	- Export as csv
	- Convert to json, delete the webscraper meta keys
	- Change the cart_img key to id, change all the values (from e.g. "069.jpg" to "069")
	- Sort by id
	- Make sure quotes `"` are replaced with `&quot;` (`\"` doesn't cut it!)

- Images (carts, logos) can be downloaded by using tools like [WebCopy](https://www.cyotek.com/cyotek-webcopy):
	- Website example: `https://famicase.com/20/index.html`
	- Add rules to only download the images:
		- `.*`, Exclude
		- `.jpg`, Include (or whatever types the images are)

---

**Step-by-step: adding new year's data:**

- Copy/paste the year folder and rename it
- Add link to the new year in the navbar on all html pages
- Add download link to the front page
- Update the JS year constants on the new year page
- Empty the img folder, and copy new images over
	- Compress all the images (Affinity Photo -> New Batch Job)
	- Logos: 100x100, 50% jpg quality
	- Carts:
		- 2020+: 820x540, 75% jpg quality
		- 2019-: No resize, 75% jpg quality
- Update the carts_data JS object with the new text data
	- Make sure the key names match the JS code
- Update the JS code to account for the potentially different image naming convention

:D
